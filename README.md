# SGF

 Sistema para gerenciamento de uma facção

## Como rodar
----
### Requerimentos
* Python 3
* Django > 2.0
* Pillow
* Pyserial

As versões mais novas do django *requerem* que o django seja rodado em um virtualenv, para criar, utilizar um virtualenv além das outras dependências do projeto, use:
```
$ pip install virtualenv
```
Nesse ponto você já deve ir para a pasta em que o projeto foi clonado
```
$ virtualenv env
$ env\Scripts\activate
$ pip install -r requerimentos.txt
$ python manage.py runserver # Para rodar o servidor web
$ python servidor.py # Para rodar a ponte entre o arduino e o banco de dados
```

Se já seguiu os passos de antes e só deseja rodar o projeto use:
```
$ env\Scripts\activate
$ python manage.py runserver
$ python servidor.py
```
## Contas
----
#### Gerente
* Login: gerente
* Senha: sgf.1234

## Observações
----
O django não aceita senhas 100% númericas, devido a isso, por enquanto, todas as senhas de costureiros devem começar com **sgf.**, antes de entrar em produção isso será concertado.