#include "Arduino.h"
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>
#include <SPI.h>
#include <EEPROM.h>
#include <avr/wdt.h>  
#include <SoftwareSerial.h>


#define _TAMANHO_CANAL 3
#define _TAMANHO_ENDERECOS 2
#define _CSN_PIN  10
#define _CE_PIN  9
#define _TIME_INTERVAL_WAY_ANALYSIS 100
#define _MIN_RANDOM_WAY_ANALYSIS 10
#define _MAX_RANDOM_WAY_ANALYSIS 100
#define _TIME_MAX_WAIT_ACK 2000
#define _DELAY_WAIT_TENTATIVAS_ACK 100
#define _ATTEMPTS_TO_ANALYSE_THE_CHANNEL 15
#define _SIZE_PAYLOAD 1
#define  _EEPROM_SOMA_PECAS_LOW  36
#define  _EEPROM_SOMA_PECAS_HIGH  37

#define _SERIAL_SIMULADO_RX A0
#define _SERIAL_SIMULADO_TX A1
#define _PORTA_SOMA_PECAS A2


int _QUEM_SOU_EU = 23;
boolean _TOW_LOGADO = false;
int _MY_ADDR = 0;
int _TO_ADDR = 0;
int _CHANNEL = 90;
char _NET_NAME[_TAMANHO_ENDERECOS];
char _NET_PASS[_TAMANHO_ENDERECOS];
int soma_pecas = 0;
SoftwareSerial serialVirtual(_SERIAL_SIMULADO_RX, _SERIAL_SIMULADO_TX); // RX, TX

void setup() {
    pinMode(_PORTA_SOMA_PECAS, OUTPUT);
    digitalWrite(_PORTA_SOMA_PECAS, HIGH);
    
    Serial.begin(9600);
    delay(2000);
    Serial.println("Inicializando...");
    Serial.println("Carregando dados...");
    saveConfig(10, 11, 90);
    loadConfig();
    configurarMirf(); 
    Serial.println("Configuracao finalizada...");
}

char c = '-';
void loop() {
  // put your main code here, to run repeatedly:
  //Serial.println("Cliente");
  //transmit(0b00000111);
  //Serial.println(0b00000111);
  if (Serial.available() > 0 )  {
    c = (char) Serial.read();
    Serial.print("Voce escolheu a opcao: "); Serial.println(c);
    if (c == 'a') {
      Serial.println("00-Configurando comunicação/Digite 000 para sair a qualquer momento");
      Serial.println("01-Configurando comunicação/Digite o nome da RADDR + TADDR + CANAL");
      String nova_configuracao = Serial.readString() ;
      
      while (true) {
        if (nova_configuracao.length() > 1) {
          if (nova_configuracao.length() == 4 && nova_configuracao.toInt() == 0) {
            break;
          }
          String novo_raddr = nova_configuracao.substring(0,_TAMANHO_ENDERECOS);
          String novo_taddr = nova_configuracao.substring(_TAMANHO_ENDERECOS, _TAMANHO_ENDERECOS * 2);
          String novo_canal = nova_configuracao.substring(_TAMANHO_ENDERECOS * 2, (_TAMANHO_ENDERECOS * 2) + _TAMANHO_CANAL);
    
          Serial.print("02-Configurando comunicação/");
          Serial.println(novo_raddr);
          Serial.print("02-Configurando comunicação/" );
          Serial.println(novo_taddr);
          Serial.print("02-Configurando comunicação/" );
          Serial.println( novo_canal);

          saveConfig(novo_raddr.toInt(), novo_taddr.toInt(), novo_canal.toInt());
          Serial.print("03-Reiniciando sistema/" );
          Serial.println(novo_canal);
          resetarAtmega();
          break;
        } else {
          nova_configuracao = Serial.readString() ;
        }
      }
    } else if (c == 'b') {
      Serial.println("10-Configurando funcionario");
      Serial.println("11-Digite o id do usuario com no minimo 3 numeros - para sair digite 000");
      String nova_funcionario = Serial.readString() ;
      
      while (true) {
        if (nova_funcionario.length() > 1) {
          if (nova_funcionario.length() == 4 && nova_funcionario.toInt() == 0) {
            break;
          }
           _QUEM_SOU_EU = nova_funcionario.toInt() ;
           Serial.print("13-Salvo o novo funcionario: " );
           Serial.println(_QUEM_SOU_EU );
           break;
        } else {          
          nova_funcionario = Serial.readString() ;
        }
      }
    } else if (c == 'p') {
        Serial.println("Segue as configuracoes atuais");
        Serial.print("31-Meu endereço/" );
        Serial.println( _MY_ADDR);
        Serial.print("32-Para endereco/" );
        Serial.println( _TO_ADDR);
        Serial.print("33-Meu canala/" );
        Serial.println( _CHANNEL);
    } else if (c == 't') {
        Serial.println("Digite a mensagem que voce enviar... se for 000 sairemos");
        while (true) {
          String nova_configuracao = Serial.readString() ;
          if (nova_configuracao.length() > 3 && nova_configuracao.toInt() == 0) {
            break;
          } else if (nova_configuracao.length() > 1){
            Serial.print("Transmitindo: "); Serial.println(nova_configuracao.toInt());
            transmit(nova_configuracao.toInt());
            delay(500);
            break;
          }
          Serial.println("Esperando inteiro para transmissao");
          delay(500);
        }
    } else if (c=='r') {
        Serial.println("Ativando modo de recebimento");
        while (true) {
          byte * b = (byte *) malloc( sizeof(byte));
          if (reciveMessage(b)) {
            Serial.println(*b);
          }
          free(b);
           Serial.println("Servidor");
          delay(200);
        }
    } else if (c == 'f') {
        Serial.println("Comecando configuracao...");
        String config = serialVirtual.readString() ;
        serialVirtual.print(config);
    } else if (c == 'l') {
        Serial.println(soma_pecas);
    } else if (c == 'm') {
        soma_pecas = 0;
        writeInt( _EEPROM_SOMA_PECAS_LOW, _EEPROM_SOMA_PECAS_HIGH, soma_pecas); 
    }
  } 

  if (digitalRead(_PORTA_SOMA_PECAS) == HIGH) {
     soma_pecas++;
     writeInt(_EEPROM_SOMA_PECAS_LOW, _EEPROM_SOMA_PECAS_HIGH, soma_pecas);    
  }
  delay(500); 
}

void resetarAtmega() {
  wdt_enable(WDTO_30MS); while(1) {};
}

/**
 *  
 *  
 *  Configurar MIR 
 *  
 *  
 */
void configurarMirf() {
    Mirf.spi = &MirfHardwareSpi;
    Mirf.cePin = _CE_PIN;
    Mirf.csnPin = _CSN_PIN;
    Mirf.init();
    Mirf.channel = _CHANNEL;
    Mirf.setRADDR( (byte *) _MY_ADDR );;
    Mirf.setTADDR( (byte *) _TO_ADDR);     
    Mirf.payload = _SIZE_PAYLOAD;
    Mirf.config();
}


/**
 * 
 * 
 * EEprom
 * 
 * 
 */
const static int _EEPROM_MY_ADDR_LOW = 0;
const static int _EEPROM_MY_ADDR_HIGH = 1;
const static int _EEPROM_NET_NAME_POINTER = 2;
const static int _EEPROM_NET_NAME_SIZE = 10;
const static int _EEPROM_NET_PASS_POINTER = 12;
const static int _EEPROM_NET_PASS_SIZE = 10;
const static int _EEPROM_MY_CHANNEL_LOW = 24;
const static int _EEPROM_MY_CHANNEL_HIGH = 25;
  const static int _EEPROM_TO_ADDR_LOW = 22;
const static int _EEPROM_TO_ADDR_HIGH = 23;

const static int _EEPROM_CHANNEL_SIZE = 3;
const static int _EEPROM_ADDR_SIZE = 5;

const static int _TYPE_BIN = 0;
const static int _TYPE_ANALOG = 1;
const static int _TYPE_CENTER = 2;
const static int _TYPE_REPEATER = 3;
const static int _TYPE_MULTI_DATA = 4;
const static int _TYPE_CENTRAL = 5;

static int  START_DEVICE_DATA = 50;


int upDateMyADDR() {
   return readInt(_EEPROM_MY_ADDR_LOW, _EEPROM_MY_ADDR_HIGH);
 }

void writeMyADDR(int addr) {
  writeInt(_EEPROM_MY_ADDR_LOW, _EEPROM_MY_ADDR_HIGH, addr);
}

int readInt(int addr_low, int addr_high) {
   byte hiByte = EEPROM.read(addr_high);; 
   byte lowByte = EEPROM.read(addr_low);
   int val = word(hiByte, lowByte);
   return val;
}

void writeInt(int addr_low, int addr_high, int val) {
  byte hiByte = highByte(val);
  byte loByte = lowByte(val); 
  EEPROM.write(addr_low, loByte);
  EEPROM.write(addr_high, hiByte); 
}

void readVectorChar(int addr, int vector_length, char *vector){  
  for(int i = addr; i < addr + vector_length; i++) {
    vector[i - addr] = EEPROM.read(i);
  }
}

void writeVectorChar(int addr, int vector_length, char *vector){  
  for(int i = addr; i < addr + vector_length; i++) {
     EEPROM.write(i, vector[i - addr]);
  }
}

void saveConfig(char name_net[10], char pass_net[10], int my_addr, int to_addr, int channel){
   writeInt(_EEPROM_MY_ADDR_LOW, _EEPROM_MY_ADDR_HIGH, my_addr);
   writeInt(_EEPROM_TO_ADDR_LOW, _EEPROM_TO_ADDR_HIGH, to_addr);
   writeInt(_EEPROM_MY_CHANNEL_LOW, _EEPROM_MY_CHANNEL_HIGH, channel);
//   writeVectorChar(_EEPROM_NET_NAME_POINTER, _EEPROM_NET_NAME_SIZE, name_net);
//   writeVectorChar(_EEPROM_NET_PASS_POINTER, _EEPROM_NET_PASS_SIZE, pass_net);
}

void saveConfig(int my_addr, int to_addr, int channel){
  Serial.println("Salvando informacoes...");
   writeInt(_EEPROM_MY_CHANNEL_LOW, _EEPROM_MY_CHANNEL_HIGH, channel);
   writeInt(_EEPROM_MY_ADDR_LOW, _EEPROM_MY_ADDR_HIGH, my_addr);
   writeInt(_EEPROM_TO_ADDR_LOW, _EEPROM_TO_ADDR_HIGH, to_addr);
}

void loadConfig(){
  Serial.println("Carregando informacoes...");
  _TO_ADDR = readInt(_EEPROM_TO_ADDR_LOW, _EEPROM_TO_ADDR_HIGH);
  _MY_ADDR = readInt(_EEPROM_MY_ADDR_LOW, _EEPROM_MY_ADDR_HIGH);    
  _CHANNEL = readInt(_EEPROM_MY_CHANNEL_LOW, _EEPROM_MY_CHANNEL_HIGH);    
  readVectorChar(_EEPROM_NET_NAME_POINTER, _EEPROM_NET_NAME_SIZE, _NET_NAME);
  readVectorChar(_EEPROM_NET_PASS_POINTER, _EEPROM_NET_PASS_SIZE, _NET_PASS);
}


 
/** 
 *    
 *    
 *    Tranmissao de dados
 *    
 *    
 *    
 */
boolean transmit(byte msg) {
  Serial.println("...Entrando na funcao de transmissao");
  boolean meioVazio = false;
  while(!meioVazio){
    clearRXFIFO();
    delay(_TIME_INTERVAL_WAY_ANALYSIS);
    delay(random(_MIN_RANDOM_WAY_ANALYSIS, _MAX_RANDOM_WAY_ANALYSIS));
    meioVazio = !Mirf.dataReady();  
    Serial.println("...Loop na funcao de transmissao");
  }
  byte *b = (byte *) malloc( sizeof(byte));
  *b  = msg;
  send(b);
  free(b);
  Serial.println("...clearRXFIFO na funcao de transmissao");
  clearRXFIFO();
  Serial.println("...Fim clearRXFIFO na funcao de transmissao");
  return true;
}

void send(byte * msg) {
  Mirf.send(msg);
  while(Mirf.isSending()){
  }
}

void clearRXFIFO() {
  //while (Mirf.dataReady()){
    //Mirf.getData(msg);
  //}
      Mirf.rxFifoEmpty();
}

int byte2int(byte hiByte, byte lowByte) {
  int a = word(hiByte, lowByte);
  return  a;
}

bool reciveMessage(byte *b) {
  if(Mirf.dataReady()){ 
    Mirf.getData(b); 
    return true;   
  } 
  return false;
}


/*
 * 
 * LCD 
 * 
 */
 /*
  * 
void printLCDBigMsg(String msg) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(msg.substring(0, 15));
  lcd.setCursor(0, 1);
  lcd.print(msg.substring(16, 31));
}

void printLCDDoubleLine(String first, String second, boolean erase) {
  if (erase) {
    lcd.clear();
  }
  printLCDFirstLine(first, false);
  printLCDSecondLine(second, false);
}

void printLCDFirstLine(String first, boolean erase) {
  if (erase) {
    lcd.clear();
  }
  lcd.setCursor(0, 0 );
  lcd.print(first);
}

void printLCDSecondLine(String second, boolean erase) {
  if (erase) {
    lcd.clear();
  }
  lcd.setCursor(0, 1);
  lcd.print(second);
}

  */

