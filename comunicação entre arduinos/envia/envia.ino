#include <Mirf.h>
#include <MirfHardwareSpiDriver.h>
#include <MirfSpiDriver.h>
#include <nRF24L01.h>




byte slaves[6];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("iniciando conf.");
  Mirf.spi = &MirfHardwareSpi;
  Mirf.cePin = 9;
  Mirf.csnPin = 10;
  Serial.println("Iniciando Mirf");
  Mirf.init();
  Mirf.setRADDR((byte*)"slav1");
  Mirf.setTADDR((byte*)"maste");
  Mirf.payload = 32;  
  Mirf.channel = 90;
  Mirf.config();
  Serial.println("Dispositivo configurado");

  slaves[0] = 's'; //este array vai servir como endereço para os slaves
  slaves[1] = 'l';
  slaves[2] = 'a';
  slaves[3] = 'v';
}

bool Preenchido1 = false;
bool Preenchido2 = false;
bool Preenchido3 = false;

bool invalido = true;

String opcao;

int QUEM_EU_SOU1;
int QUEM_EU_SOU2;
int QUEM_EU_SOU3;

int mensagem1;
int mensagem2;

int Nmaquina;

String ler =(byte*) malloc(6); 

int aux;

int producao = 0;

byte data[32];


void loop() {


  /*
   * 
   * comando
   * 
   */

  while(Serial.available() == 0 or (opcao.toInt()>6 or opcao.toInt()<1)){
    if(opcao.toInt() == 0){
      Serial.println("escolha umas das opções:");
      Serial.println("1 - Login");
      Serial.println("2 - Logout");
      Serial.println("3 - Incrementar");
      Serial.println("4 - Decrementar");
      Serial.println("5 - Produção atual");
      Serial.println("6 - Buscar produção\n");
      }
      
    while(Serial.available() == 0){} //espera o usuario digitar algo
    
    opcao = Serial.readString();

    if(opcao.toInt()>6 or (opcao.toInt()<1 and opcao.toInt()!=0)){ //verifica se o valor digitado é maior que 6 ou menor que 1
      Serial.println("digite uma opção válida...\n");
      }
    else if(opcao.toInt()==0){} //se o valor digitado for 0 ele retorna as opções disponiveis
    else{break;}
    }

  
  data[0] = opcao.toInt(); //salva o valor digitado na lista

  /*
   * 
   * leitura do cpf
   * 
   */

  if(!Preenchido1 and opcao.toInt() == 1){ //verifica se o usuario já digitou o CPF antes
   
    while(true){
      if(Serial.available()>0 and Serial.available()!=11){ //se o numero digitado nao tiver 11 digitos
        Serial.println("digite um cpf válido...");
        ler = Serial.readString();
        }
      else if(Serial.available() == 0){
        Serial.println("digite seu cpf...");
        }
      else{break;}

      while(Serial.available() == 0){ //espera o usuario digitar algo
        delay(100); //obs: sem esse delay o arduino não consegue ler a serial por completo, já que ele vai estar lendo muito rápido
        }
      }
      
    ler = Serial.readString();
  
    QUEM_EU_SOU1 = ((String)ler[0]+ler[1]+ler[2]+ler[3]).toInt(); //separando o cpf em 3 variaveis inteiras
    QUEM_EU_SOU2 = ((String)ler[4]+ler[5]+ler[6]+ler[7]).toInt();
    QUEM_EU_SOU3 = ((String)ler[8]+ler[9]+ler[10]).toInt();

  }

  data[1] = highByte(QUEM_EU_SOU1); //passando cada um dos 6 bytes do cpf
  data[2] = lowByte(QUEM_EU_SOU1);
  data[3] = highByte(QUEM_EU_SOU2);
  data[4] = lowByte(QUEM_EU_SOU2);
  data[5] = highByte(QUEM_EU_SOU3);
  data[6] = lowByte(QUEM_EU_SOU3); 

  /*
   * 
   * mensagem
   * 
   */

  
  if(opcao.toInt() == 1 or opcao.toInt() == 2){ //login e logout
    
    if(opcao.toInt() == 1 and Preenchido1 == true){ //verifica se já tem algum login em execução na maquina
      Serial.println("esta máquina já fez login no sistema, para acessar outra conta faça logout");
      invalido = true;
      }

    else if(opcao.toInt() == 2 and Preenchido1 == false){ //verifica se já tem algum login em execução na maquina
      Serial.println("esta máquina não fez login no sistema, para acessar uma conta faça login");
      invalido = true;
      }

    else{
      while(true){ 
        if(Serial.available() == 0){
        Serial.println("digite sua senha...\n");
          }
        else if(Serial.available()!=8){
          Serial.println("a senha deve conter 8 digitos\n");
          ler = Serial.readString();
          }
        else{
          ler = Serial.readString();
          break;
          }
          
        while(Serial.available() == 0){ //espera o usuario digitar algo
          delay(100); //obs: sem esse delay o arduino não consegue ler a serial por completo, já que ele vai estar lendo muito rápido
          }
        }
  
       mensagem1 = ((String)ler[0]+ler[1]+ler[2]+ler[3]).toInt();
       mensagem2 = ((String)ler[4]+ler[5]+ler[6]+ler[7]).toInt();
  
      
  
      data[7] = highByte(mensagem1); //separa a msg em 4 byts
      data[8] = lowByte(mensagem1);
      data[9] = highByte(mensagem2);
      data[10] = lowByte(mensagem2);

      if(opcao.toInt() == 1){Preenchido2 = true;}
      else if(opcao.toInt() == 2){
        Preenchido2 = false;
        Preenchido1 = false;
        }

      invalido = false;
      }
    }
  

  else if(opcao.toInt() == 3){ //incrementar
    if(Preenchido1 == true){
      producao += 1;
      data[7] = 0;
      data[8] = 0;
      data[9] = 0;
      data[10] = 0;
    }
    else{ //caso não tenha nenhum login em execução na maquina
      
      Serial.println("faça login no sistema para utilizar este comando");
      invalido = true;
      
      }
  }

  else if(opcao.toInt() == 4){ //decrementar
    if(Preenchido1 == true){
      producao -= 1;
      data[7] = 0;
      data[8] = 0;
      data[9] = 0;
      data[10] = 0;
    }
    else{ //caso não tenha nenhum login em execução na maquina
      
      Serial.println("faça login no sistema para utilizar este comando");
      invalido = true;
      
      }
    }

  else if(opcao.toInt() == 5 or opcao.toInt() == 6){ //buscar produção
    if(Preenchido1 == true){
      data[7] = 0;
      data[8] = 0;
      data[9] = highByte(producao);
      data[10] = lowByte(producao);
    }
    else{ //caso não tenha nenhum login em execução na maquina
      
      Serial.println("faça login no sistema para utilizar este comando");
      invalido = true;
      
      }
    }

  /*
   * 
   * número da máquina
   * 
   */

  if(!invalido){ //executa caso não tenha nenhum problema nos passos anteriores
    while(true){
      if(Serial.available() == 0){
        Serial.println("digite o número da máquina...\n");
        }
      else if((Serial.available() == 1 or Serial.available() == 2) or Serial.available() == 3){ //verifica se o numero digitado contem 1, 2 ou 3 digitos
        aux = Serial.available();
        ler = Serial.readString();
        break;
        }
      else{ //executa se o numero digitado não tiver o numero requisitado de digitos (1, 2 ou 3)
        Serial.println("digite um numero válido...\n");
        ler = Serial.readString();
        }
        
      while(Serial.available() == 0){ //espera o usuario digitar algo
        delay(100); //obs: sem esse delay o arduino não consegue ler a serial por completo, já que ele vai estar lendo muito rápido
        }
        
      }
    
    if(aux == 1){
      Nmaquina = ((String)ler[0]).toInt();
      }
    else if(aux == 2){
      Nmaquina = ((String)ler[0]+ler[1]).toInt();
      }
    else{
      Nmaquina = ((String)ler[0]+ler[1]+ler[2]).toInt();
      }
  
    //definindo o endereço de recebimento
    slaves[4] = highByte(Nmaquina);
    slaves[5] = lowByte(Nmaquina);

    Mirf.setRADDR((byte*)slaves);
    Mirf.config();
    
    data[11] = highByte(Nmaquina);
    data[12] = lowByte(Nmaquina);

  
    /*
     * 
     * Bytes sobrando
     * 
     */
  
    for(int i = 13; i < 32; i++){
      data[i] = 0;
      }
    
    send(data);
    delay(500);
  
    
    /*
     * 
     * verificação de login
     * 
     */
  
    if(opcao.toInt() == 1){ 
      while(true){
        if(!Mirf.isSending() && Mirf.dataReady()){
          Mirf.getData((byte*)&data);
          break;
        }
      }
      if(data[0] == 1){ //verifica se o numero retornado foi 1
        Serial.println("login concluido com sucesso...");
        Preenchido1 = true;
        }
      else{
        Serial.println("falha no login, verifique se o cpf e a senha estão corretos...");
        Preenchido1 = false;
        }
      
      }
    opcao = "0";
  }
  else{ //executa caso tenha ocorrido algum problema nas etapas anteriores
    Serial.println("envio cancelado\n");
    opcao = "0";
    invalido = false;
    }
  
}


int _TIME_INTERVAL_WAY_ANALYSIS = 100; //
int _MIN_RANDOM_WAY_ANALYSIS = 10; //
int _MAX_RANDOM_WAY_ANALYSIS = 100; //

typedef struct msg_simple{
    byte _hops;
    byte _id_msg;
    int _from_address;
    int _to_address;
    byte _length_payload;
    byte _payload[25];
};

void clearRXFIFO() {
    byte msg[sizeof(msg_simple)];
    while (Mirf.dataReady()){
        Mirf.getData(msg);
    }
}


void send(byte * msg) {
  
  boolean meioVazio = false;
  while(!meioVazio){
    clearRXFIFO();
    delay(_TIME_INTERVAL_WAY_ANALYSIS);
    delay(random(_MIN_RANDOM_WAY_ANALYSIS, _MAX_RANDOM_WAY_ANALYSIS));
    meioVazio = !Mirf.dataReady();  
  }

  Mirf.send(msg);
  while(Mirf.isSending()){}
}



