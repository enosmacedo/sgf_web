#include <Mirf.h>
#include <MirfHardwareSpiDriver.h>
#include <MirfSpiDriver.h>
#include <nRF24L01.h>

byte slaves[5]; //corrigir utilizando uma lista de bytes

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("iniciando conf.");
  Mirf.spi = &MirfHardwareSpi;
  Mirf.cePin = 9;
  Mirf.csnPin = 10;
  Serial.println("Iniciando Mirf");
  Mirf.init();
  Mirf.setRADDR((byte *)"maste");
  Mirf.setTADDR((byte *)"slav2");
  Mirf.payload = 32;  
  Mirf.channel = 90;
  Mirf.config();
  Serial.println("Dispositivo configurado");

  slaves[0] = 's';
  slaves[1] = 'l';
  slaves[2] = 'a';
  slaves[3] = 'v';
}

String lerSerial;


void loop() {
  
// enviando para a serial
  byte data[32];

  
  if(!Mirf.isSending() && Mirf.dataReady()){
    
    Mirf.getData((byte*)&data);

    for(int i = 0; i < 32; i++){ 
      for(int j = 7; j>-1; j--){
        Serial.print(bitRead(data[i], j)); //escrevendo os bits de cada byte
        }
      }
    Serial.println("");

    /*
     * 
     * verificação do login
     * 
     */

    if(data[0] == 1){

      slaves[4] = data[11];
      slaves[5] = data[12];

      Mirf.setTADDR((byte*)slaves);
      Mirf.config();
      
      while(true){
        if(Serial.available() == 1){
           lerSerial = Serial.readString(); 
           break;
          }
        }

     
      data[0] = lerSerial.toInt();
  
      for(int i = 1; i < 32; i++){
        data[i] = 0;
        }
  
      send(data);
      }
    
    }
 
  
}

void send(byte * msg) {
  Mirf.send(msg);
  while(Mirf.isSending()){
   }
}
