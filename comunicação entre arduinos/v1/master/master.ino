#include <Mirf.h>
#include <MirfHardwareSpiDriver.h>
#include <MirfSpiDriver.h>
#include <nRF24L01.h>

byte maquina[5] = {109, 97, 113, 117, 0};
bool debug = false;

void setup() {
  Serial.begin(115200);

  Mirf.spi = &MirfHardwareSpi;
  
  Mirf.cePin = 9;
  Mirf.csnPin = 10;
  
  Mirf.init();
  
  Mirf.setRADDR((byte *)"serv0"); 
  Mirf.setTADDR((byte *)&maquina); 
  Mirf.payload = 16;  
  
  Mirf.channel = 110;
  
  Mirf.config();

  Serial.println("----- Master -----");
}

void loop(){
  byte c[Mirf.payload];
  
  if(Mirf.dataReady())
  {
    Mirf.getData((byte *)&c);

    if (c[12] != 0) { return; } // Se não for de um slave
    if (c[11] == 0) { return; } // Se não vier se ninguém
    if (c[13] != 0) { return; } // Provavelmente interferência
    if (!pacoteValido((byte *)c)) { return; }

    for (int i = 0; i < Mirf.payload; i++)
    {
      for(int x = 7; x > -1; x--)
      {
          Serial.print(bitRead(c[i], x));
      }        
    }
    Serial.print('\n');

    // Maquina
    byte maq = c[11];
    maquina[4] = maq;
    Mirf.setTADDR((byte *)&maquina);   
    
    while(Serial.available() != 1) { if (Serial.available() > 1) { Serial.readString(); } };
    byte resposta[Mirf.payload];
    resposta[13] = (bool)Serial.readString().toInt();
    resposta[11] = maq;
    resposta[12] = 1;
    
    Mirf.send((byte *)&resposta);
    while(Mirf.isSending());
    
    //Mirf.flushRx();
  }
}

/**
    Limpa todos os dados que foram recebidos quando não se esperava dados.

    @param Nenhum.
    @return Vazio.
*/
void clearOldData()
{
   byte data[Mirf.payload];
   while(Mirf.dataReady()) { Mirf.getData((byte *)&data); Serial.println("Tinha algo no clear");}
}

void getAddr() 
{
  uint8_t address[5];
  Mirf.readRegister(0x10, (uint8_t *)&address, 5);
  Serial.print("Address: ");
  Serial.print((char)address[0]);
  Serial.print((char)address[1]);
  Serial.print((char)address[2]);
  Serial.print((char)address[3]);
  Serial.print(address[4]);
  Serial.print('\n');
}

bool pacoteValido(byte * c)
{
  for (int i = 14; i < Mirf.payload; i++)
  {
    if (c[i] != 0) { return false; }  
  }
  return true;
}
