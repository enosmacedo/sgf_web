#include <Mirf.h>
#include <MirfHardwareSpiDriver.h>
#include <MirfSpiDriver.h>
#include <nRF24L01.h>

#define TIMEOUT 5000
#define MIN_RANDOM 500
#define MAX_RANDOM 2000
#define MAQUINA 5
#define DEBUG true

byte maquina[5] = {109, 97, 113, 117, MAQUINA};

bool isLogado = false;
byte CPF[6];

void setup() {
  Serial.begin(9600);

  Mirf.spi = &MirfHardwareSpi;
  
  Mirf.cePin = 9;
  Mirf.csnPin = 10;
  
  Mirf.init();

  Mirf.setRADDR((byte *)&maquina); 
  
  Mirf.payload = 16;  
  
  Mirf.channel = 110;
  
  Mirf.config();

  Mirf.setTADDR((byte *)"serv0");
}

void loop(){
  byte c[Mirf.payload];

  mostrarBanner();
  mostrarMenu((byte *)&c);
}

void mostrarBanner() {
  Serial.print("-------- Maquina ");Serial.print(MAQUINA);Serial.println(" --------");
  Serial.println("1 - Login");
  if (isLogado) { Serial.println("2 - Logout"); }
  if (isLogado) { Serial.println("3 - Incrementar"); }
  if (isLogado) { Serial.println("4 - Decrementar"); }
  if (isLogado)  { Serial.println("5 - Produção total"); }
  Serial.println("6 - Mostrar status");
  Serial.println("7 - Mudar endereço");
  if (isLogado) { Serial.println("Você está logado!"); }
}

void mostrarMenu (byte * c) {
    while(!Serial.available());
    switch(Serial.read())
    {
      case '1': login(c, false); break;
      case '2': logout(c, false); break;
      case '3': incrementar(c, false); break;
      case '6': mostrarStatus(c); break;
      case '7': mudarAddr(c); break;
      default: Serial.println("Digite uma opção válida");
    }
}

/**
    Faz o login do usuário

    @param (c) Ponteiro para o buffer de dados que será enviado.
           (dataSet) Bool, quando true nenhum dado é perguntado novamente. 
    @return Vazio.
*/
void login(byte * c, bool dataSet)
{
     if (!dataSet) {
       c[0] = 1;
       Serial.println("Digite seu cpf.");
       while(true)
       {
        if(Serial.available() > 0 && Serial.available() != 11) { Serial.println("Digite um cpf válido com 11 digitos."); Serial.readString(); } else if (Serial.available() == 0) { delay(15); } else { break; }
         delay(15);
       }
       
       String ler = Serial.readString();
       
       Serial.println("Digite sua senha.");
       while(true)
       {
        if(Serial.available() > 0 && Serial.available() != 8) { Serial.println("A senha precisa ter 8 digitos"); Serial.readString(); } else if (Serial.available() == 0) { delay(15); } else { break; }
         delay(15);
       }
  
       String senha = Serial.readString();
  
       // CPF
       int CPF1 = ((String)ler[0] + ler[1] + ler[2] + ler[3]).toInt(); 
       int CPF2 = ((String)ler[4] + ler[5] + ler[6] + ler[7]).toInt(); 
       int CPF3 = ((String)ler[8] + ler[9] + ler[10]).toInt();
  
       c[1] = highByte(CPF1);
       c[2] = lowByte(CPF1);
       c[3] = highByte(CPF2);
       c[4] = lowByte(CPF2);
       c[5] = highByte(CPF3);
       c[6] = lowByte(CPF3);

       CPF[0] = c[1];
       CPF[1] = c[2];
       CPF[2] = c[3];
       CPF[3] = c[4];
       CPF[4] = c[5];
       CPF[5] = c[6];
       CPF[6] = c[7];
       
       // Senha - Mensagem
       int SENHA1 = ((String)senha[0] + senha[1] + senha[2] + senha[3]).toInt();
       int SENHA2 = ((String)senha[4] + senha[5] + senha[6] + senha[7]).toInt();
  
       c[7] = highByte(SENHA1);
       c[8] = lowByte(SENHA1);
       c[9] = highByte(SENHA2);
       c[10] = lowByte(SENHA2);
     }
     
     //Envio
     clearOldData();
     sendData((byte *)c);
     
     if (waitForData(login, (byte *) c, true, 0)) { return; }
}

/**
    Faz o logout do usuário

    @param (c) Ponteiro para o buffer de dados que será enviado.
           (dataSet) Bool, quando true nenhum dado é perguntado novamente. 
    @return Vazio.
*/
void logout(byte * c, bool dataSet)
{
  if (!isLogado) { Serial.println("Você não está logado!"); return; }
  if (!dataSet) {
     c[0] = 2;
     
     loadCPF(c);
     
     c[7] = 0;
     c[8] = 0;
     c[9] = 0;
     c[10] = 0;
  }

  clearOldData();
  sendData((byte *)c);
     
  if (waitForData(logout, (byte *) c, false, 0)) { return; }
  
  Serial.println("Você saiu!");
  isLogado = false;
  clearOldData();
}

/**
    Faz o logout do usuário

    @param (c) Ponteiro para o buffer de dados que será enviado.
           (dataSet) Bool, quando true nenhum dado é perguntado novamente. 
    @return Vazio.
*/
void incrementar(byte * c, bool dataSet)
{
  if (!isLogado) { Serial.println("Você não está logado!"); return; }
  if (!dataSet) {
     c[0] = 3;
     
     loadCPF(c);
     
     c[7] = 0;
     c[8] = 0;
     c[9] = 0;
     c[10] = 0;

     clearOldData();
  } else {
   delay(random(MIN_RANDOM, MAX_RANDOM));  
  }
  
  
  sendData((byte *)c);
     
  if (waitForData(incrementar, (byte *) c, false, 0)) { return; }

  //if (!validarData()) { Serial.println("Pacote Inválido"); incrementar((byte *)c, true); return; }
  
  Serial.println("Sucesso!");
  //clearOldData();
}

void mostrarStatus (byte * c)
{
   c[0] = 6;
   sendData(c);
   Serial.println("Enviando pedido de status");
}

void mudarAddr (byte * c)
{
  c[0] = 7;
  Serial.println("Digite o endereço");
  while(!Serial.available());
  c[1] = (byte)Serial.readString().toInt();
  sendData(c);
  Serial.println("Pedido de mudança enviado com sucesso!");
}

/**
    Limpa todos os dados que foram recebidos quando não se esperava dados.

    @param Nenhum.
    @return Vazio.
*/
void clearOldData()
{
   byte data[Mirf.payload];
   while(Mirf.dataReady()) { Mirf.getData((byte *)&data); }
}

/**
    Envia os dados pelo Mirf

    @param Buffer dos dados a serem enviados.
    @return Vazio.
*/
void sendData(byte * c)
{
  fillUnused(c);
  c[11] = MAQUINA;
  c[12] = 0;
  Mirf.send((byte *)c);
  while(Mirf.isSending());
}

/**
    Preenche o resto do buffer com bytes de valor 0

    @param Buffer dos dados a serem enviados.
    @return Vazio.
*/
void fillUnused(byte * c)
{
  for(int i = 12; i < Mirf.payload; i++)
  {
    c[i] = 0;  
  }
}

/**
    Espera por uma resposta do 

    @param Buffer dos dados a serem enviados.
    @return Vazio.
*/
bool waitForData(void (*f) (byte *,  bool), byte * c, bool eLogin, int tentativas) {

    // Verificando se o servidor respondeu
    unsigned long before = millis();
    while(!Mirf.dataReady()) { if (millis() - before > TIMEOUT) { Serial.println("O servidor não respondeu, tentando novamente!"); f((byte *)c, true); return true; } delay(10); }
    delay(50);

    // Resposta
    byte response[Mirf.payload];
    Mirf.getData((byte *)&response);

    // Verificando se o pacote deveria ser para a gente, se não, espera um pouco
    if (response[11] != MAQUINA) {
      Serial.println("Pacote inválido, tentando novamente");
      delay(TIMEOUT);
      if (tentativas > 3) { f((byte *)c, true); return true; }
      return waitForData(f, (byte *) c, eLogin, tentativas++);
    }

    
    if (response[12] != 1) {
      Serial.println("Pacote não veio do servidor!");
      delay(TIMEOUT);
      f((byte *)c, true); return true;
      
    }
    
    if (eLogin) { if (response[13] == 1) { Serial.println("Logado com sucesso!"); isLogado = true; } else { Serial.println("Login incorreto"); isLogado = false; } return true; }
    
    if (response[13] != 1) { Serial.println("Resposta negativa"); ((byte *)c, true); return true; }
    return false;
}

/**
    Serve para carregar o CPF na mensagem

    @param Buffer dos dados a serem enviados.
    @return Vazio.
*/
void loadCPF(byte * c)
{
  c[1] = CPF[0];
  c[2] = CPF[1];
  c[3] = CPF[2];
  c[4] = CPF[3];
  c[5] = CPF[4];
  c[6] = CPF[5];
}

bool validarData()
{
    byte response[Mirf.payload];
    Mirf.getData((byte *)&response);
    return (response[11] == MAQUINA);
}
