/* 
************************
******* Master ********
************************
* Autor: José Levi
* Data: 05/08/2018
************************
*/

#include <Mirf.h>
#include <MirfHardwareSpiDriver.h>
#include <MirfSpiDriver.h>
#include <nRF24L01.h>

/* Configurações */
#define PAYLOAD 32
#define TOTAL_CHECAR 5
#define TIMEOUT 500

/* Variáveis Globais */
byte maquinas[TOTAL_CHECAR + 1];

/* Constantes */
#define NEWDATA 0xFF
#define NODATA 0xEF
#define PING 0x0F
#define LOGIN 1
#define LOGOUT 2
#define VAZIO 0

void setup() {
    Serial.begin(115200);
  
    Mirf.spi = &MirfHardwareSpi;
    Mirf.cePin = 9;
    Mirf.csnPin = 10;
    Mirf.init();
    Mirf.setRADDR((byte *)"serv0");
    Mirf.payload = PAYLOAD;
    Mirf.channel = 110;
    Mirf.config();
}

void loop() {
  ping();
  perguntar();
}

void ping()
{
  Mirf.flushRx();
  byte dados[PAYLOAD];
  for (int i = 1; i < (TOTAL_CHECAR + 1); i++)
  {
      dados[11] = (byte)i;
      dados[0] = PING;
      setAddr(i);
      Mirf.send((byte *)&dados);
      while(Mirf.isSending());

      unsigned long antes = millis();
      while(!Mirf.dataReady()) { if (millis() - antes > TIMEOUT) { break; } }
      if (Mirf.dataReady()) {
        Mirf.getData((byte *)&dados);
        if (dados[11] == (byte)i) {
          maquinas[i] = 1;
        }
      }
  }  
}

void perguntar()
{
  for (int i = 1; i < (TOTAL_CHECAR + 1); i++)
  {
    if (maquinas[i] == 1)
    {
        Mirf.flushRx();
        byte dados[PAYLOAD];

        // Endereçamento
        dados[11] = (byte)i;
        dados[0] = NEWDATA;
        setAddr(i);

        Mirf.send((byte *)&dados);
        while(Mirf.isSending());
        unsigned long antes = millis();
        while(!Mirf.dataReady()) { if (millis() - antes > TIMEOUT) { break; } }
        if (Mirf.dataReady()) {
            Mirf.getData((byte *)&dados);

            if(i != dados[11]) { Serial.println("Alguém mandou algo onde não era sua vez, ignorando"); continue; }
            
//            for (int x = 0; x < PAYLOAD; x++)
//            {
//              for (int z = 7; z > -1; z--)
//              {
//                Serial.print(bitRead(dados[x], z));  
//              }  
//            }
//            Serial.print('\n');
  
            if (dados[0] == LOGIN)
            {
              Serial.print("Requisição de login: ");
              Serial.println(dados[11]);
              dados[1] = 1;
              Mirf.send((byte *)&dados);
              while(Mirf.isSending());
              
            } else if (dados[0] == NODATA ) {

            } else if (dados[0] == PING) {
              Serial.print("Ping fora de tempo: ");  
              Serial.println(dados[11]);
            } else if (dados[0] == LOGOUT) {
              Serial.print("Requisição de logout: ");
              Serial.println(dados[11]);
              Mirf.send((byte *)&dados);
              while(Mirf.isSending());
            }
        }
    }  
  }  
}

void setAddr(int n)
{
  byte addr[5] = {'m', 'a', 'q', 'u', (byte)n}; 
  Mirf.setTADDR((byte *)&addr);
}
