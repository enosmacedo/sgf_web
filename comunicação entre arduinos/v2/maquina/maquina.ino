/* 
************************
******* Maquina ********
************************
* Autor: José Levi
* Data: 05/08/2018
************************
*/

#include <Mirf.h>
#include <MirfHardwareSpiDriver.h>
#include <MirfSpiDriver.h>
#include <nRF24L01.h>

/* Configurações */
#define PAYLOAD 32 // Tamanho do payload da mensagem | Só números pares
#define MAXFILA 5 // Tamanho da fila de comando TODO: Alocar dinâmicamente
#define TIMEOUT 3000
#define MAQUINA 2

/* Variáveis Globais */
byte data[PAYLOAD];
bool isLogado = false;
char CPF[11];
byte FILA[MAXFILA][PAYLOAD];
byte maquina[5] = {'m', 'a', 'q', 'u', MAQUINA};
bool waitingLogin = false;
bool waitingLogout = false;

/* Constantes */
#define NEWDATA 0xFF
#define NODATA 0xEF
#define PING 0x0F
#define LOGIN 1
#define LOGOUT 2
#define VAZIO 0

void setup() {
    Serial.begin(115200);
  
    Mirf.spi = &MirfHardwareSpi;
    Mirf.cePin = 9;
    Mirf.csnPin = 10;
    Mirf.init();
    Mirf.setRADDR((byte *)&maquina);
    Mirf.payload = PAYLOAD;
    Mirf.channel = 110;
    Mirf.config();
    Mirf.setTADDR((byte *)"serv0");
}

void loop() {
    mostrarMenu();
    esperarInput();
}

/*
 * Mostar o menu pelo Serial
 * 
 * @params Nenhum
 * @return Vazio
*/
void mostrarMenu ()
{
    Serial.println("----- Menu -----");
    if (!isLogado && !waitingLogin) { Serial.println("1 - Logar");       }
    if (isLogado && !waitingLogout)  { Serial.println("2 - Logout");      }
    if (isLogado && !waitingLogout)  { Serial.println("3 - Incrementar"); }
    if (isLogado && !waitingLogout)  { Serial.println("4 - Decrementar"); }
}

/*
 * Espera pelo input do usuário via Serial e trata
 * 
 * @params Nenhum
 * @return Vazio
*/
void esperarInput()
{
    while(!Serial.available()) { checarFila(); delay(10); } // Enquanto não tem nenhum comando, fica esperando por informações do master
    delay(10);
    switch(Serial.read()) {
       case '1': login(); break;
       case '2': logout(); break;
       default: Serial.println("Opção inválida!"); break;
    }
}

/*
 * Trata o processo de login
 * 
 * @params Nenhum
 * @return Vazio
*/
void login()
{  
    if (isLogado || waitingLogin ) { return; }
    Serial.println("Digite seu CPF");
    while (Serial.available() < 11) { if (Serial.available() != 0) {  Serial.print("O CPF precisa ter 11 dígitos, você digitou "); Serial.print(Serial.available()); Serial.println(" dígitos, escreva novamente!"); Serial.readString(); } delay(10); }
    String cpf = Serial.readString();
    Serial.println("Digite sua SENHA");
    while (Serial.available() < 8) { if (Serial.available() != 0) {  Serial.print("A SENHA precisa ter 8 dígitos, você digitou "); Serial.print(Serial.available()); Serial.println(" dígitos, escreva novamente!"); Serial.readString(); } delay(10); }
    String senha = Serial.readString();

    cpf.toCharArray((char *) &CPF, 12);
   
    carregarCPF();

    
    char senhaArray[senha.length()];
    senha.toCharArray(senhaArray, sizeof(senhaArray) + 1);
    carregarMensagem(senhaArray, sizeof(senhaArray));

    data[0] = LOGIN;

    Serial.println("Logando... Pode demorar um pouco");
    waitingLogin = true;
    enviar();
}

void logout()
{
    if (!isLogado || waitingLogout) { return; }
    carregarCPF();
    data[0] = LOGOUT;
    waitingLogout = true;
    enviar();
}

/*
 * Adiciona o conteudo de data na fila, se não tiver espaço descarta
 * 
 * TODO: Adicionar realocamento dinâmico, para aumentar o tamanho da FILA
 * 
 * @params Nenhum
 * @return Vazio
*/
void enviar()
{
   int tamanho = (int)(sizeof(FILA) / PAYLOAD);
   bool colocou = false;
   for (int x = 0; x < tamanho;x++) 
   {
      if (FILA[x][0] == VAZIO) {
        setarFila(x);
        colocou = true;
        break;
      }
   }
   if (!colocou) { Serial.println("Fila cheia"); }
}

/*
 * Checa se na fila tem algo para ser enviado ao master,
 * só deve ser executado se estiver na vez. Caso tenha algo, 
 * é enviado automáticamente.
 * 
 * @params Nenhum
 * @return False se tinha dados
 *         True se não tinha dados para serem tratados
*/
bool checarFila()
{
    if (Mirf.dataReady())
    {
      byte info[PAYLOAD];
      Mirf.getData((byte *)&info);
      
      if (!validarPacote((byte *)&info)) { return false; }
      
      byte tipo = info[0];
      byte filaTipo = 0; 
      int i = 0;
      if (tipo == PING)
      { 
         info[1] = 1;
      } else if (tipo == NEWDATA) {
         info[0] = NODATA;
         for (i = 0; i < MAXFILA; i++)
         {
            byte filaTipo = FILA[i][0];
            if(filaTipo != 0) { fetchFila((byte *)&info, i); break; }
         }
      } else {
        return false;  
      }
      sendData((byte *)&info);
      filaTipo = info[0];
      if (filaTipo == LOGIN) { handleLogin(i); ; }
      else if (filaTipo == LOGOUT){ handleLogout(i); }
      return true;
    } else {
      return false;  
    }
}

void handleLogin(int pos) {
   unsigned long antes = millis();
   while (!Mirf.dataReady()) { if (millis() - antes > TIMEOUT) { return; } }
   byte c[PAYLOAD];
   Mirf.getData((byte *)&c);
   if (!validarPacote((byte *)&c)) { Serial.println("Pacote inválido"); return; }
   if (c[0] != LOGIN) { return; }
   if (c[1] == 1) { Serial.println("Login bem sucedido!"); waitingLogin = false; isLogado = true; } else { Serial.println("CPF ou SENHA incorreto(s)!"); }
   limparInfo(pos);
   mostrarMenu();
}

void handleLogout(int pos) {
  unsigned long antes = millis();
  while (!Mirf.dataReady()) { if (millis() - antes > TIMEOUT) { return; } }
  byte c[PAYLOAD];
  Mirf.getData((byte *)&c);
  if (!validarPacote((byte *)c)) { Serial.println("Pacote inválido"); return; }
  if (c[0] == LOGOUT) { isLogado = false; waitingLogout = false; Serial.println("Deslogado com sucesso!"); limparInfo(pos); }
  mostrarMenu();
}

/*
 * Envia do dados no buffer passado
 * 
 * @params Ponteiro do buffer passado
 * @return Vazio
*/
void sendData(byte * c)
{
  c[11] = MAQUINA;
  Mirf.send((byte *)c);
  while(Mirf.isSending());
}

/*
 * Pega dados da fila
 * 
 * @params Ponteiro do buffer onde os dados estarão
 *         Posição da fila para pegar
 * @return Vazio
*/
void fetchFila(byte * c, int pos)
{
  for (int i = 0; i < PAYLOAD; i++)
  {
    c[i] = FILA[pos][i];
  }
}


/*
 * Carrega o CPF no buffer de dados
 * 
 * @params Nenhum
 * @return Vazio
*/
void carregarCPF()
{
    int CPF1 = ((String) CPF[0] + CPF[1] + CPF[2] + CPF[3]).toInt();
    int CPF2 = ((String) CPF[4] + CPF[5] + CPF[6] + CPF[7]).toInt();
    int CPF3 = ((String) CPF[8] + CPF[9] + CPF[10]).toInt();
  
    data[1] = highByte(CPF1);
    data[2] = lowByte(CPF1);
    data[3] = highByte(CPF2);
    data[4] = lowByte(CPF2);
    data[5] = highByte(CPF3);
    data[6] = lowByte(CPF3);
}


/*
 * Carrega a mensagem no buffer de dados
 * 
 * @params Ponteiro para a mensagem
 *         Tamanho da mensagem (Máximo 8)
 * @return Vazio
*/
void carregarMensagem(char * msg, int len)
{
    String PARTE1S;
    for (int i = 0; i < 4; i++)
    {
        PARTE1S += (String)msg[i];
    }
    int PARTE1 = PARTE1S.toInt();
    
    data[7] = highByte(PARTE1);
    data[8] = lowByte(PARTE1);
    
    if (len > 4) 
    {
      String PARTE2S;
      for (int i = 4; i < 8; i++)
      {
          PARTE2S += (String)msg[i];
      }
      int PARTE2 = PARTE2S.toInt();

      data[9] = highByte(PARTE2);
      data[10] = lowByte(PARTE2);
    } else {
      data[9] = 0x00;
      data[10] = 0x00;
    }
}

/*
 * Completa a lista com algumas informações
 * 
 * @params Lugar da fila onde colocar os dados
 * @return Vazio
*/
void setarFila(int place)
{
  for(int i = 0; i < PAYLOAD; i++)
  {
    FILA[place][i] = data[i];  
  }
}

/*
 * Limpa um lugar da fila
 * 
 * @params Lugar da fila onde os dados seram limpos
 * @return Vazio
*/
void limparInfo(int place)
{
    FILA[place][0] = 0;  
}

bool validarPacote(byte * c)
{
  return (c[11] == MAQUINA);  
}
