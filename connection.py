import serial # Conexão serial
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SGF.settings")
import django
django.setup()
from dashboard.models import UserProduction, Extra
from django.db.models import F
from datetime import datetime
from django.contrib.auth import authenticate

if __name__ == '__main__':
	# Constantes
	LOGIN = 1
    LOGOUT = 2
    INCREMENTAR = 3
    DECREMENTAR = 4
    PRODUCTION_ATUAL = 5
    BUSCAR_PRODUCTION = 6


    # Pegar data compativel com a do BD
    def getDate():
    	return datetime.now().strftime('%Y-%m-%d')

    # Essa função NÃO deve ser acessada diretamente
    def checarProduction(user):
    	try:
    	    return UserProduction.objects.get(user=user, data=getDate())
    	except UserProduction.DoesNotExist:
    	    prd = UserProduction(user=user, data=getDate())
    	    prd.save()
    	    return prd

    # Atualizar peça no banco de dados (Usuário DEVE existir)
    def adicionar(user, quantidade=1):
    	prd = checarProduction(user)
    	prd.quantidade = F('quantidade') + 1
    	prd.save()

    def remover(user, quantidade=1):
    	prd = checarProduction(user)
    	if prd.quantidade > 0:
    		prd.quantidade = F('quantidade') - 1
    	prd.save()

    def decode(binario):
    	return {
    		'comando': int(binario[0:8], 2),
    		'cpf': ''.join([str(int(binario[x:x+16], 2)) for x in range(8, 56, 16)]),
    		'mensagem': ''.join([str(int(binario[56:72], 2)), str(int(binario[72:88] ,2))]),
    		'maquina': str(int(binario[88:104], 2))
    	}

    # Autentica o usúario, retora True se o usuário existir e asenha estiver correta
    # Retorna False se o usuário não existir, ou se ele existir mas a senha está incorreta
    def autenticar(cpf, senha):
        try:
            funcionario = Extra.objects.get(cpf=cpf)
            return funcionario.user.check_password('sgf.'+senha)

        except Extra.DoesNotExist:
    	    return False

    arduino = serial.Serial('COM4', 9600)

    autenticados = []

    while True:
        dados = arduino.readline()
        novodado = ""
        vd = []
        dados = str(dados)

        for i in dados:
            vd.append(i)


        for i in range(len(vd)) :
            if not ((vd[i] == "\\") and (vd[i+1] == "r")):
                if not ((vd[i] == "r") and (vd[i-1] == "\\")):
                    if not i == len(vd)-1:
                        if not((vd[i] == "b") and (vd[i+1] =="'")):
                            if not ((vd[i] == "\\") and (vd[i+1] == "n")):
                                if not ((vd[i] == "n") and (vd[i-1] == "\\")):
                                    if not((vd[i] == "'") and (vd[i-1] =="b")):
                                        novodado=novodado+vd[i]




        try:
	        msg = decode(novodado)
	        cmd = msg['comando']
	        cpf = msg['cpf']
	        if cmd == LOGIN:
	        	if autenticar(msg['cpf'], msg['mensagem']):
	        		print('CPF', msg['cpf'], 'Autenticado')
	        		autenticados.append(msg['cpf'])
	        	else:
	        		print('CPF não autenticado')
	        elif cmd == LOGOUT:
	        	if cpf in autenticados:
	        		autenticados.remove(cpf)
	        		print("{} saiu.".format(cpf))
	        elif cmd == INCREMENTAR:
	        	if cpf in autenticados:
	        		adicionar(Extra.objects.get(cpf=cpf))
	        		print("{} incrementou 1.".format(cpf))
	        elif cmd == DECREMENTAR:
	        	if cpf in autenticados:
	        		remover(Extra.objects.get(cpf=cpf))
	        elif cmd == PRODUCTION_ATUAL:
	        	print(msg)
	        elif cmd == BUSCAR_PRODUCTION:
	        	print(msg)
	        else:
	        	print('Inconsistente')
        except ValueError:
            print(novodado)
        
    arduino.close()
    
    #try:
        

    #except Extra.DoesNotExist:
    #    print("Usuário não existe")
    
