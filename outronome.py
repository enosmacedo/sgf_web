import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SGF.settings")

import django

django.setup()

from dashboard.models import UserProduction, Extra # Modelos
from django.db.models import F # Função para incrementar sem perigo de duplicação

from datetime import datetime # Biblioteca para checar a data

from django.contrib.auth import authenticate # Autenticar usuário

if __name__ == '__main__':
	
	# Pegar data compativel com a do BD
	def getDate():
		return datetime.now().strftime('%Y-%m-%d')

	# Essa função NÃO deve ser acessada diretamente
	def checarProduction(user):
		try:
			return UserProduction.objects.get(user=user, data=getDate())
		except UserProduction.DoesNotExist:
			prd = UserProduction(user=user, data=getDate())
			prd.save()
			return prd

	# Atualizar peça no banco de dados (Usuário DEVE existir)
	def adicionar(user, quantidade=1):
		prd = checarProduction(user)
		prd.quantidade = F('quantidade') + 1
		prd.save()


	def remover(user, quantidade=1):
		prd = checarProduction(user)
		prd.quantidade = F('quantidade') - 1
		prd.save()

	def decode(binario):
		return {
			'comando': int(msg[0:8], 2),
			'cpf': ''.join([str(int(msg[x:x+16], 2)) for x in range(8, 56, 16)]),
			'mensagem': ''.join([str(int(msg[56:72], 2)), str(int(msg[72:88] ,2))]),
			'maquina': str(int(msg[88:104], 2))
		}

	# Autentica o usúario, retora True se o usuário existir e asenha estiver correta
	# Retorna False se o usuário não existir, ou se ele existir mas a senha está incorreta
	def autenticar(cpf, senha):
		try:
			funcionario = Extra.objects.get(cpf=cpf)
			return funcionario.user.check_password(senha)

		except Extra.DoesNotExist:
			return False

	try:


	  # usuario = Extra.objects.get(cpf='70208608427')
	  # adicionar(usuario)

	  # print(UserProduction.objects.all()[1].quantidade)

	except 
		print('Usuário não existe')
