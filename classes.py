from dashboard.models import UserProduction, Extra, Sessao
from django.db.models import F
from django.utils import timezone
from django.contrib.auth import authenticate

class LoginNaoExiste(Exception):
	pass

class SenhaIncorreta(Exception):
	pass

class UsuarioNaoAutenticado(Exception):
	pass

class Costureiro:
	def __init__(self, comunicacao):
		self.cpf = comunicacao.cpf
		#self.maquina = comunicacao.maquina
		self.costureiro = None
		self.sessao = None

	def getDate(self):
		return timezone.localtime().strftime('%Y-%m-%d')

	def autenticar(self, senha):
		if self.autenticado():
			self.logout()

		self.senha = 'sgf.' + senha
		try:
			funcionario = Extra.objects.get(cpf=self.cpf)
			self.costureiro = funcionario
		except Extra.DoesNotExist:
			raise LoginNaoExiste('CPF não existe no banco de dados.')
		else:
			if not funcionario.user.check_password(self.senha):
				raise SenhaIncorreta('Senha incorreta')
			else: 
				sessao = Sessao.objects.create(funcionario=self.costureiro)
				sessao.save()
				return True

	def autenticado(self):
		sessao = Sessao.objects.filter(funcionario__cpf=self.cpf, logout_hora=None) # Apenas sessões ativas
		total = sessao.count()
		if total > 0:
			self.sessao = sessao.order_by('login_hora')[0]
			self.costureiro = self.sessao.funcionario
			return True
		else:
			return False

	def logout(self):
		if not self.autenticado():
			raise UsuarioNaoAutenticado('O usuário não pode fazer logout se ele não está autenticado')

		else:
			self.sessao.logout_hora = timezone.now()
			self.sessao.save()
			self.sessao = None
			self._cleanSessions()


	# Só pra garantir que não fique nenhuma sessão em aberto
	# SO USAR SE NÃO FOR ESPERADO NENHUMA SESSÃO ATIVA EM ABERTO
	def _cleanSessions(self):
		for sessao in Sessao.objects.filter(funcionario__cpf=self.cpf, logout_hora=None):
			sessao.delete()


	def checarProduction(self):
		try:
			return UserProduction.objects.get(user=self.costureiro, data=self.getDate())
		except UserProduction.DoesNotExist:
			prd = UserProduction(user=self.costureiro, data=self.getDate())
			prd.save()
			return prd

	def incrementar(self, qnt=1):
		prd = self.checarProduction()
		prd.quantidade = F('quantidade') + qnt
		prd.save()

	def remover(self, qnt=1):
		prd = self.checarProduction()
		if prd.quantidade >= qnt:
			prd.quantidade = F('quantidade') - qnt
		prd.save()



class Comunicação:
	LOGIN = 1
	LOGOUT = 2
	INCREMENTAR = 3
	DECREMENTAR = 4
	PRODUCTION_ATUAL = 5
	BUSCAR_PRODUCTION = 6
	DESCONHECIDO = 0

	def __init__(self, binario, debug = True):
		binario = binario.decode('utf-8').rstrip()
		if len(binario) is not 128:
			self.comando = 0
			self.cpf = None
			self.mensagem = binario
			self.maquina = None
		else:
			self.comando = int(binario[0:8], 2)
			self.cpf = ''.join([str(int(binario[x:x+16], 2)) for x in range(8, 56, 16)])
			self.mensagem = ''.join([str(int(binario[56:72], 2)), str(int(binario[72:88] ,2))])
			self.maquina = str(int(binario[88:96], 2))

	def __str__(self):
		return str({'comando': self.comando, 'cpf': self.cpf, 'mensagem': self.mensagem, 'maquina': self.maquina})