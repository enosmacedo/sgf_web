import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SGF.settings")
import django
django.setup()


from classes import Costureiro, Comunicação, LoginNaoExiste, SenhaIncorreta, UsuarioNaoAutenticado
from dashboard.models import Sessao
from serial import Serial, serialutil

if __name__ == '__main__':
	while True:
		try:
			arduino = Serial(input('Insira a porta em que o arduino está conectado (Ex: COM4):').upper(), 115200, timeout=5, write_timeout=5)
		except serialutil.SerialException:
			print('Não existe nenhum dispositivo conectado a essa porta, verifique a porta e tente novamente')
		else:
			break

	while True:
		if not arduino.inWaiting():
			continue

		dado = arduino.readline()
		com = Comunicação(dado)

		cmd = com.comando

		if cmd is Comunicação.LOGIN:
			funcionario = Costureiro(com)

			try:
				funcionario.autenticar(com.mensagem)
			except LoginNaoExiste:
				# CPF não consta no banco de dados
				print('CPF de {} não existe!'.format(funcionario.cpf))
				arduino.write(b'0')
			except SenhaIncorreta:
				# CPF existe porém está incorreto
				print('Senha de {} está incorreta!'.format(funcionario.cpf))
				arduino.write(b'0')
			else:
				print('Login bem sucedido de {}'.format(funcionario.cpf))
				arduino.write(b'1')
		elif cmd is Comunicação.LOGOUT:
			funcionario = Costureiro(com)

			try:
				funcionario.logout()
			except UsuarioNaoAutenticado:
				# O funcionario está tentando fazer logout sem estar autenticado
				print('O funcionario {} tentou fazer logout sem estar autenticado'.format(funcionario.cpf))
			else:
				print('O funcionario {} acabou de sair'.format(funcionario.cpf))

			arduino.write(b'1')

		elif cmd is Comunicação.INCREMENTAR:
			funcionario = Costureiro(com)
			
			if funcionario.autenticado():
				funcionario.incrementar()
				print('O funcionario {} acabou de incrementar 1 peça'.format(funcionario.cpf))
				arduino.write(b'1')
			else:
				arduino.write(b'0')
				print('O funcionario {} não estava autenticado'.format(funcionario.cpf))
			

		elif cmd is Comunicação.DECREMENTAR:
			funcionario = Costureiro(com)
			
			if funcionario.autenticado():
				funcionario.remover()
				print('O funcionario {} acabou de decrementar 1 peça'.format(funcionario.cpf))
				arduino.write(b'1')
			else:
				arduino.write(b'0')

		elif cmd is Comunicação.PRODUCTION_ATUAL:
			pass
		elif cmd is Comunicação.BUSCAR_PRODUCTION:
			pass
		elif cmd is Comunicação.DESCONHECIDO:
			print(com.mensagem)

		
