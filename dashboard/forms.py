from django import forms
from django.contrib.auth.models import User, Group
from .models import Extra
from django.contrib.auth.password_validation import validate_password

# Formulário para upload de arquivo do ponto eletrônico
class uploadFileForm(forms.Form):
	arquivo = forms.FileField(widget=forms.FileInput(attrs={'accept': '.txt'}), label='')

# Formulário para criação de usuários baseado na classe User do django
class createUserForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(createUserForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True
		self.fields['first_name'].required = True
		self.fields['last_name'].required = True

	def clean_email(self):
		email = self.cleaned_data.get('email')
		username = self.cleaned_data.get('username')
		qs = User.objects.filter(email=email)
		print(qs)
		if self.instance:
			qs = qs.exclude(pk=self.instance.pk)
		if qs.count():
			raise forms.ValidationError('Esse email já está em uso')
		else:
			return self.cleaned_data['email']
	def clean_password(self):
		password = self.cleaned_data['password']
		if validate_password(password):
			raise forms.ValidationError(validate_password(password))
		return password

	class Meta:
		model = User
		fields = ['first_name', 'last_name', 'email', 'username', 'password']
		widgets = {
			'password': forms.PasswordInput(),
		}
		help_texts = {
			'username': ''
		}
		error_messages = {
			'password': {
				'min_length': 'O CPF precisa ter 11 digitos'
			}
		}

		
	grupo = forms.ModelChoiceField(queryset=Group.objects.all())

	def save(self, commit=False):
		user = super(createUserForm, self).save(commit=True)
		user.set_password(self.cleaned_data['password'])
		if commit:
			user.save()
		return user

# Formulário para criação das informações extras do funcionário basedo na classe Extra do arquivo models.py
class createExtraForm(forms.ModelForm):
	class Meta:
		model = Extra
		fields = ['cpf', 'avatar', 'pis']
		widgets = {
			'cpf': forms.TextInput(attrs={'placeholder': '00000000000',}),
			'pis': forms.TextInput(attrs={'placeholder': '000000000000',})
		}
		labels = {
			'cpf': 'CPF',
			'pis': 'PIS'
		}
		error_messages = {
			'cpf': {
				'unique': 'CPF em uso',
				'min_length': 'O CPF precisa ter 11 digitos'
			},
			'pis': {
				'unique': 'PIS em uso',
				'min_length': 'O PIS precisa ter 12 digitos'

			}
		}