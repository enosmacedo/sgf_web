from django.contrib import admin

from .models import Extra, Ponto, FuncionarioMes, UserProduction, MelhoresFuncionarios, HorasTrabalhadas

# Register your models here.
admin.site.register(Extra)
admin.site.register(Ponto)
admin.site.register(FuncionarioMes)
admin.site.register(UserProduction)
admin.site.register(MelhoresFuncionarios)
admin.site.register(HorasTrabalhadas)