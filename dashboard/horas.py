import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SGF.settings")
import django
django.setup()
from .models import Ponto, Extra, HorasTrabalhadas
from datetime import datetime
from django.db.models import F # Função para incrementar sem perigo de duplicação


def verificar(user, mes, ano):
	try:
		return HorasTrabalhadas.objects.get(user=user, mes=mes, ano=ano)
	except HorasTrabalhadas.DoesNotExist:
		h = HorasTrabalhadas(user=user, mes=mes, ano=ano, horas=float(0))
		h.save()
		return h


def somar_hora(user, inicio, fim, mes, ano):
	print(ano, mes, inicio, fim)
	ht = verificar(user, mes, ano)
	f = '%H:%M:%S'
	dif = (datetime.strptime(str(fim), f) - datetime.strptime(str(inicio), f)).total_seconds()
	ht.horas = ht.horas + (dif/3600)
	print(ht.horas)
	ht.save()