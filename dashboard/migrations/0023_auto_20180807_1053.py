# Generated by Django 2.0.7 on 2018-08-07 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0022_auto_20180729_1811'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ponto',
            name='horaSaida',
            field=models.TimeField(default='00:00'),
        ),
    ]
