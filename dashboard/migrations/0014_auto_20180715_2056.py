# Generated by Django 2.0 on 2018-07-15 23:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0013_auto_20180715_2053'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extra',
            name='cpf',
            field=models.CharField(default='00000000000', max_length=11, unique=True, verbose_name='cpf'),
        ),
    ]
