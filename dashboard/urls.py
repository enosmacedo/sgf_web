from django.urls import path, register_converter, include
from django.contrib.auth import views as auth_views

from django.conf import settings
from django.conf.urls.static import static

from . import views, converters

register_converter(converters.DataConverter, 'data')

urlpatterns = [
	path('', views.index, name='mainClean'),
	path('main', views.index, name="main"),
	path('logar', auth_views.login, name="login"),
	path('logout', auth_views.logout, name="logout"),
	path('search', views.search, name='procurar'),
	path('estatisticas', views.estatisticas, name='estatisticas'),
	path('controle', views.controle, name='controle'),
	path('controle/<int:tipo>/<int:ordem>/<data:de>/<data:ate>', views.controle_pesquisa, name='c_pesquisa'),
	path('uploadFilePonto', views.uploadFilePonto, name='uploadFilePonto'),
	path('salario/<int:mes>/<int:ano>', views.salario, name='salario'),
	path('createUser', views.createUser, name='createUser'),
	path('delete_user/<int:id>', views.delete_user, name='delete_user'),
	path('edit_user/<int:id>', views.edit_user, name='edit_user'),
	path('conta/', include('django.contrib.auth.urls')),
	path('live', views.live, name='live'),
	path('melhores', views.melhoresFuncionariosDia, name='melhores'),
	path('grafico', views.grafico, name='grafico'),
	path('producao_pdf/<int:id>', views.producao_pdf, name='producao_pdf'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
