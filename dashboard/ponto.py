import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SGF.settings")
import django
django.setup()
from .models import Ponto, Extra
from .horas import somar_hora
import datetime

# Faz a leitura do arquivo do ponto eletrônico
# Salva os pontos no banco de dados
# Caso o usuário não exista, salva o ponto em um outro lugar para ser salvo no banco de dados depois (Falta implementar)
# Se o funcioário não bateu o ponto de saída, é adicionado a hora padrão (17:00)
def main(arquivo):
    erros = []
    try:
        lista = open(arquivo,'r').read().splitlines()
        tamanho = len(lista)
        matriz=[]
        for i in range(tamanho):
            vetor = []
            iden=""
            re = ""
            cont = 1
            dia = ""
            mes = ""
            ano = ""
            hora = ""
            minuto = ""
            pis = ""
            
            if i >0 :
                for j in lista[i]:
                
                    if cont <=9:
                        iden = iden+j
                    if cont == 10:
                        re = j
                    
                    if cont >10 and cont <=12:
                        dia = dia+j
                    if cont >12 and cont <=14:
                        mes = mes+j
                    if cont >14 and cont <=18:
                        ano = ano+j
                    if cont >18 and cont <=20:
                        hora = hora+j
                    if cont >20 and cont <=22:
                        minuto = minuto+j
                    if cont >22 :
                        pis = pis+j
                    cont = cont+1
                data = datetime.date(int(ano), int(mes), int(dia)).strftime('%Y-%m-%d')
                sec = 00
                hora = datetime.time(int(hora), int(minuto), sec).strftime('%H:%M:%S')
                try:
                    u = Extra.objects.get(pis=pis)
                    try:
                        p = Ponto.objects.get(user=u, data=data, horaSaida='00:00')
                        p.horaSaida = hora
                        p.save()
                        print("Saiu", u, '-', hora)
                        somar_hora(u, p.horaEntrada, p.horaSaida, p.data.month, p.data.year)
                    except Ponto.DoesNotExist:
                        p = Ponto(user=u, data=data, horaEntrada=hora)
                        p.save()
                        print("Entrou", u, '-', hora)
                except Extra.DoesNotExist:
                    erros.append("O PIS " + pis + " não está cadastrado no sistema")
        pon = Ponto.objects.filter(horaSaida='00:00')
        if pon:
            for i in pon:
                i.horaSaida='17:00:00'
                i.save()
                somar_hora(i.user, i.horaEntrada, i.horaSaida, i.data.month, i.data.year)
        return erros
    except Exception as e:
        erros.append("Um erro inesperado aconteceu, verifique o arquivo! ERRO: " + str(e))
        return erros
