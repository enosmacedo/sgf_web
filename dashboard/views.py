from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User, Group
from django.http import JsonResponse, HttpResponse
from django.utils import timezone
from operator import itemgetter
from .ponto import main
from .forms import uploadFileForm, createUserForm, createExtraForm
from .models import UserProduction, FuncionarioMes, MelhoresFuncionarios, Extra, Ponto
from django.contrib import messages
from django.db.models import Sum
from .utils import render_to_pdf


# Monta a tela inicial do sistema com detalhe das contas ativas, produção mensal, funcionário do mês e os melhores funcionários
@login_required
def index(request):
	since = timezone.localtime() - timezone.timedelta(30)
	lastMonth = timezone.localtime().replace(day=1) - timezone.timedelta(days=1)

	totalProduzido = UserProduction.objects.filter(data__gte=since.strftime('%Y-%m-%d')).aggregate(total=Sum('quantidade'))['total'] # ~13% mais rápido do que a antiga forma

	try:
		funcionarioMes = FuncionarioMes.objects.get(data__month=lastMonth.month)
	except FuncionarioMes.DoesNotExist:
		total = []
		for costureiro in User.objects.filter(groups__name='Costureiro'):
			produzido = sum([x.quantidade for x in UserProduction.objects.filter(user__user=costureiro)])
			total.append({'funcionario': costureiro, 'produzido': produzido})
	
		maior = max([x['produzido'] for x in total])
		for a in total:
			if a['produzido'] == maior:
				funcionarioMes = FuncionarioMes()
				funcionarioMes.funcionario = a['funcionario']
				funcionarioMes.total = a['produzido']
				funcionarioMes.media = 0 # Implementar com o sistema de pontos
				funcionarioMes.data = lastMonth
				funcionarioMes.save()
	
	# Não testado ainda
	if not MelhoresFuncionarios.objects.filter(data__month=lastMonth.month).count() > 0 and UserProduction.objects.filter(data__month=lastMonth.month).count() > 0:
		total = []
		for costureiro in User.objects.filter(groups__name='Costureiro'):
			produzido = sum([x.quantidade for x in UserProduction.objects.filter(user__user=costureiro)])
			total.append({'funcionario': costureiro, 'produzido': produzido})

		for n in range(5):
			print('Entrou no loop')
			if not len(total) > 0:
				break
			maior = max([x['produzido'] for x in total])
			for a in total:
				if a['produzido'] == maior:
					funcionario = MelhoresFuncionarios()
					funcionario.funcionario = a['funcionario']
					funcionario.total = a['produzido']
					funcionario.media = 0 # Implementar com o sistema de pontos
					funcionario.data = lastMonth
					funcionario.save()
					total.remove(a)
	else:
		melhoresFuncionarios = MelhoresFuncionarios.objects.filter(data__month=lastMonth.month).order_by('total')

	dados = {
		'gerente': User.objects.filter(groups__name='Gerente').count(),
		'secretário': User.objects.filter(groups__name='Secretário').count(), 
		'costureiro': User.objects.filter(groups__name='Costureiro').count(),
		'desde': since.strftime('Desde %d/%m/%y'),
		'mhora': round(totalProduzido / 720, 2),
		'mdia': round(totalProduzido / 30, 2),
		'mtotal': totalProduzido,
		'funcionario_mes': funcionarioMes,
		'melhores_funcionarios': melhoresFuncionarios
	}
	return render(request, 'dashboard/index.html', {'grupo': request.user.groups.all()[0], 'nome': request.user.first_name, 'img': request.user.extra.avatar.url, 'dados' : dados})

def search(request):
	pass

@login_required
def estatisticas(request):
	return render(request, 'dashboard/estatisicas.html', {'grupo': request.user.groups.all()[0], 'nome': request.user.first_name, 'img': request.user.extra.avatar.url})

# Constroi a tela que mostra todos os funcionários
@login_required
def controle(request):
	funcionarios = User.objects.exclude(first_name="Admnistrador")
	tempo = 5
	return render(request, 'dashboard/controle.html', {'grupo': request.user.groups.all()[0], 'nome': request.user.first_name, 'img': request.user.extra.avatar.url, 'funcionarios': funcionarios, 'tempo': tempo})


@login_required
def controle_pesquisa(request, tipo, ordem, de, ate):
	tempo = 5
	de = '-'.join(de.split('/')[::-1])
	ate = '-'.join(ate.split('/')[::-1])


	# Registro
	if tipo == 1 and ordem == 1:
		funcionarios = User.objects.exclude(first_name="Admnistrador").filter(date_joined__range=[de, ate])
	elif tipo == 1 and ordem == 2:
		funcionarios = User.objects.exclude(first_name="Admnistrador").filter(date_joined__range=[de, ate]).order_by('-date_joined')
	elif tipo == 2:
		dic = {}
		for func in User.objects.exclude(first_name="Admnistrador"):
		   media = sum([x.quantidade in func.extra.userproduction_set.filter(data__range=[de, ate])]) / (timezone.localtime().strptime(ate, '%Y-%m-%d') - timezone.localtime().strptime(de, '%Y-%m-%d'))
		   dic.update({func: media})
		if ordem == 1:
			funcionarios = [x[0] for x in sorted(dic.items(), key=itemgetter(1))]
		else:
			funcionarios = [x[0] for x in sorted(dic.items(), key=itemgetter(0))]
	else:
		funcionarios = User.objects.exclude(first_name="Admnistrador")


	return render(request, 'dashboard/controle.html', {'grupo': request.user.groups.all()[0], 'nome': request.user.first_name, 'img': request.user.extra.avatar.url, 'funcionarios': funcionarios, 'tempo': tempo})


# ---------------------- Função para salvar o arquivo -------------------
def handle_uploaded_file(f):
	with open('dashboard/ponto/ponto.txt', 'wb+') as destination:
		for chunk in f.chunks():
			destination.write(chunk)
# -----------------------------------------------------------------------

# Salva o arquivo de registro do ponto eletrônico e salva, no banco de dados, os pontos presentes nele
@login_required
def uploadFilePonto(request):
	if not(request.user.groups.filter(name='Gerente').exists() or request.user.groups.filter(name='Secretario').exists()):
		return redirect('main')
	if request.method == 'POST':
		form = uploadFileForm(request.POST, request.FILES)
		if form.is_valid():
			handle_uploaded_file(request.FILES['arquivo'])
			pont = main('dashboard/ponto/ponto.txt')
			if pont==[]:
				messages.success(request, 'O arquivo foi salvo com sucesso!')
			return render(request, 'dashboard/novo_ponto.html', {'grupo': request.user.groups.all()[0], 
					'nome': request.user.first_name, 'img': request.user.extra.avatar.url, 'erros':pont, 'form':form})

	else:
		form = uploadFileForm()
		return render(request, 'dashboard/novo_ponto.html', {'grupo': request.user.groups.all()[0], 
					'nome': request.user.first_name, 'img': request.user.extra.avatar.url, 'form': form})

@login_required
def salario(request, mes, ano):
	func = Extra.objects.all()
	dias_trabalhados = Ponto.objects.filter(data__year=str(ano), data__month=str(mes))
	for i in dias_trabalhados:
		print(i)
	return render(request, 'dashboard/salario.html', {'grupo': request.user.groups.all()[0], 
					'nome': request.user.first_name, 'img': request.user.extra.avatar.url, 'func': func})

# Cadastro de funcionários
@login_required
def createUser(request):
	if not(request.user.groups.filter(name='Gerente').exists() or request.user.groups.filter(name='Secretário').exists()):
		messages.error(request, 'Você não tem permissão para realizar essa ação')
		return redirect('main')

	if request.method == 'POST':
		form1 = createUserForm(request.POST)
		form2 = createExtraForm(request.POST, request.FILES)

		if form1.is_valid() and form2.is_valid():
			user_form = form1.save(commit=False)
			extra_form = form2.save(commit=False)
			user = user_form.username
			user_form.save()
			u = User.objects.get(username=user)
			my_group = Group.objects.get(name=str(form1.cleaned_data['grupo'])) 
			my_group.user_set.add(u)
			extra_form.user = User.objects.get(username=user)
			extra_form.save()
			messages.success(request, 'Usuário cadastrado com sucesso: %s' %user)
			return redirect('controle')
		else:
			return render(request, 'dashboard/createUser.html', {'grupo': request.user.groups.all()[0], 
					'nome': request.user.first_name, 'img': request.user.extra.avatar.url, 'form1': form1, 'form2': form2})
	else:
		return render(request, 'dashboard/createUser.html', {
			'grupo': request.user.groups.all()[0], 
			'nome': request.user.first_name, 
			'img': request.user.extra.avatar.url, 
			'form1': createUserForm(), 
			'form2': createExtraForm()
		})

# Exclui funcionários
@login_required
def delete_user(request, id):
	if not(request.user.groups.filter(name='Gerente').exists() or request.user.groups.filter(name='Secretário').exists()):
		messages.error(request, 'Você não tem permissão para realizar essa ação')
		return redirect('main')
	user = User.objects.get(pk=id)
	nome = user.username
	user.delete()
	messages.success(request, 'O usuário %s foi removido do sistema!' %nome)
	return redirect('controle')

# Edita os dados dos funcionários 
@login_required
def edit_user(request, id):
	if not(request.user.groups.filter(name='Gerente').exists() or request.user.groups.filter(name='Secretário').exists()):
		messages.error(request, 'Você não tem permissão para realizar essa ação')
		return redirect('main')
	form_user = User.objects.get(pk=id)
	form_extra = Extra.objects.get(user=id)
	if request.method == 'POST':
		form1 = createUserForm(request.POST, instance=form_user)
		form2 = createExtraForm(request.POST, instance=form_extra)
		if form1.is_valid() and form2.is_valid():
			user_form = form1.save(commit=False)
			extra_form = form2.save(commit=False)
			user = user_form.username
			user_form.save()
			u = User.objects.get(username=user)
			u.groups.clear()
			my_group = Group.objects.get(name=str(form1.cleaned_data['grupo'])) 
			my_group.user_set.add(u)
			extra_form.user = User.objects.get(username=user)
			extra_form.save()
			messages.success(request, 'Usuário atualizado com sucesso: %s' %user)
			return redirect('controle')
		else:
			return render(request, 'dashboard/edit_user.html', {'grupo': request.user.groups.all()[0], 
					'nome': request.user.first_name, 'img': request.user.extra.avatar.url, 'form1': form1, 'form2': form2})
	else:
		form1 = createUserForm(instance=form_user)
		form2 = createExtraForm(instance=form_extra)
		return render(request, 'dashboard/edit_user.html', {'grupo': request.user.groups.all()[0], 
					'nome': request.user.first_name, 'img': request.user.extra.avatar.url, 'form1': form1, 'form2': form2})

# Produção em tempo real
@login_required
def live(request):
	if not(request.user.groups.filter(name='Gerente').exists() or request.user.groups.filter(name='Secretário').exists()):
		return redirect('main')
	return render(request, 'dashboard/live.html', { 'usuario':request.user })

# Trás uma lista com os melhores funcionários do dia
@login_required
def melhoresFuncionariosDia(request):
	if not(request.user.groups.filter(name='Gerente').exists() or request.user.groups.filter(name='Secretário').exists()):
		return redirect('main')

	hoje = timezone.localtime().strftime('%Y-%m-%d')
	usuarios = User.objects.filter(extra__userproduction__data=hoje).values('id', 'first_name', 'extra__userproduction__quantidade').order_by('-extra__userproduction__quantidade')	
	return JsonResponse({'acesso': timezone.localtime(), 'usuarios': list(usuarios)})

from random import randint

# Pega informação via AJAX
@login_required
def grafico(request):
	hoje = timezone.localtime().strftime('%Y-%m-%d')
	y = UserProduction.objects.aggregate(total=Sum('quantidade'))['total']
	return JsonResponse({'acesso': timezone.localtime().timestamp(), 'total': y})

# Pega toda a produção do funcionário
@login_required
def producao_pdf(request, id):
	if not(request.user.groups.filter(name='Gerente').exists() or request.user.groups.filter(name='Secretário').exists()):
		return redirect('main')
	u =  Extra.objects.get(user=id)
	userpro = UserProduction.objects.filter(user=u)
	data = {
		'fun': u,
		'userpro': userpro,
	}
	pdf = render_to_pdf('pdf/producao_pdf.html', data)
	return HttpResponse(pdf, content_type='application/pdf')