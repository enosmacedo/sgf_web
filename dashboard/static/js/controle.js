$(document).ready(function () {
	$('.input-append.date').datepicker({
		format: 'dd/mm/yyyy',
		language: 'pt-BR',
		endDate: 'now'
	});

	$('.dropdown-menu > li > a').click(function(e) {
		var texto = e.currentTarget.text;
		var antigo = $(this).parent().parent().parent().children('button')[0].innerHTML;
		$(this).parent().parent().parent().children('button')[0].innerHTML = texto;
		e.currentTarget.text = antigo;
	});

	$('#filterForm').click(function (e) {
		// Valida campos
		var de = $('input[name="data-de"]');
		var ate = $('input[name="data-ate"]');

		if(de.val().length == 0 || ate.val().length == 0 )
		{
			showError("Você deve preencher todos os campos");
			return;
		}

		var tipo = $('#filtro1')[0].innerHTML;
		var ordem = $('#filtro2')[0].innerHTML;

		if (tipo == "Registro") {
			tipo = 1;
		} else {
			tipo = 2;
		}

		if (ordem == "Crescente") {
			ordem = 1;
		} else {
			ordem = 2;
		}

		var url = "/controle/"+tipo+"/"+ordem+"/"+de.val().split('/').join('-')+"/"+ate.val().split('/').join('-');
		window.location.href = url; 
	});

	function showError(msg)
	{
		Messenger().post({
	 		message: msg,
	 		type: 'error',
         	showCloseButton: true
	    });	
	}
});