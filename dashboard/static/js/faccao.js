$(document).ready(function() {


	var seriesData = [ [], []];
	var random = new Rickshaw.Fixtures.RandomData(50);


	for (var i = 0; i < 50; i++) {
		random.addData(seriesData);
	}

	graph = new Rickshaw.Graph( {
		element: document.querySelector("#graficoGeral"),
		height: 200,
		renderer: 'area',
		series: [
			{
				data: seriesData[0],
				color: 'rgba(0,144,217,0.51)',
				name:'Média'
			},{
				data: seriesData[1],
				color: '#eceff1',
				name:'Produção agora'
			}
		]
	} );
	var hoverDetail = new Rickshaw.Graph.HoverDetail( {
		graph: graph
	});

	setInterval( function() {
		//var seriesData[Serie][PosDado] = {x: 'começa no 0, diferença de 50', y: 'média'}
		console.log(seriesData);
		random.removeData(seriesData);
		random.addData(seriesData);
		graph.update();

	},1000);

});