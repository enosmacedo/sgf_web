$(document).ready(function() {
	$('.top-menu-toggle-dark').click(function() {
		$('body').toggleMenu();
	});
	updateTable();
	setInterval(updateTable, 15000);

	$('.reload').click(function() {
		var el = $(this).parents(".grid");
		$.blockUI(el);
		updateTable(function() { console.log('tamo aqui'); $.unblockUI(el); });
	});

	var dados = [{'x': Date.now() / 1000, 'y': 0}];

	var grafico = new Rickshaw.Graph( {
		element: document.getElementById("graficoGeral"),
		renderer: 'area',
		dataURL: 'grafico',
		series: [
			{
				name: 'Produção agora',
				color: 'rgba(0,144,217,0.51)',
				data: dados
			}
		]
	});

	var hoverDetail = new Rickshaw.Graph.HoverDetail( {
		graph: grafico
	});
	
	grafico.render();

	var lastY = null;

	setInterval(function() {
		$.ajax({
		  method: 'GET',
		  url: '/grafico',
		  dataType: 'json'
		})
			.done(function(data) {
				if (lastY == null) {
					lastY = data.total;
					return;
				} else {
					data.total -= lastY;
				}
				dados.push({x: data.acesso, y: data.total});
				grafico.update();
				console.log(dados);
			});
	}, 5000);
});

function updateTable(callback) {
	$.ajax({
	  method: 'GET',
	  url: '/melhores',
	  dataType: 'json'
	})
		.done(function(data) {
			$('tbody').empty();
			for(var i = 0; i < data.usuarios.length; i++)
			{
				var tr = $('<tr></tr>');
				tr.append('<td>' + (parseInt(i)+1) + '</td>');
				tr.append('<td>' + data.usuarios[i].first_name + '</td>');
				tr.append('<td>' + (Math.round((parseInt(data.usuarios[i].extra__userproduction__quantidade) / new Date().getHours()) * 100) / 100) + ' peças/h</td>');
				tr.append('<td>' + data.usuarios[i].extra__userproduction__quantidade + ' peças</td>');
				$('tbody').append(tr);
			}
			if(callback) {
				console.log('aqui');
				callback();
			}
		});
}