class DataConverter:
	regex = '[0-9]{2}-[0-9]{2}-[0-9]{4}'

	def to_python(self, value):
		return value.replace('-','/', 3)

	def to_url(self, value):
		return value.replace('/','-', 3)
