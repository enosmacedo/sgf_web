from django.db import models
from django.core.validators import MinLengthValidator, RegexValidator
from django.contrib.auth.models import User
from django.dispatch import receiver
import os
from django.core import signing

# Models

def user_directory_images_path(instance, filename):
	
	return 'perfil/{0}'.format(signing.b64_encode((str(instance.user.id) + filename).encode('UTF-8')).decode('UTF-8'))

# Dados extra para o úsuario
class Extra(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	cpf = models.CharField(max_length=11, validators=[
			MinLengthValidator(11),
			RegexValidator(r'^[0-9]*$', 'Apenas números.')
		], unique=True)
	avatar = models.ImageField(upload_to=user_directory_images_path, default='perfil/default.png')
	pis = models.CharField(max_length=12, validators=[
			MinLengthValidator(12),
			RegexValidator(r'^[0-9]*$', 'Apenas números.')
		], unique=True)

	def __str__(self):
		return self.user.first_name

# Guarda a produção de um usúario, gerado todo dia
class UserProduction(models.Model):
	user = models.ForeignKey(Extra, on_delete=models.CASCADE)
	data = models.DateField(auto_now_add=True)
	quantidade = models.IntegerField(default=0)


class Ponto(models.Model):
	user = models.ForeignKey(Extra, on_delete=models.CASCADE)
	data = models.DateField(null=True)
	horaEntrada = models.TimeField(null=True)
	horaSaida = models.TimeField(default='00:00')

# Guarda os funcionários do mês
class FuncionarioMes(models.Model):
	funcionario = models.ForeignKey(User, on_delete=models.CASCADE)
	data = models.DateField()
	media = models.FloatField()
	total = models.IntegerField()

	def __str__(self):
		return self.funcionario.first_name

# Guarda os melhores funcionários, atualizado todos os meses
class MelhoresFuncionarios(models.Model):
	funcionario = models.ForeignKey(User, on_delete=models.CASCADE)
	data = models.DateField()
	media = models.FloatField()
	total = models.IntegerField()

	def __str__(self):
		return self.funcionario.first_name

# Model para guardar a sessão das maquinas
class Sessao(models.Model):
	funcionario = models.ForeignKey(Extra, on_delete=models.CASCADE)
	login_hora = models.DateTimeField(auto_now_add=True)
	logout_hora = models.DateTimeField(null=True, blank=True)

@receiver(models.signals.post_delete, sender=Extra)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deleta o arquivo do sistema
    quando o objeto de `Extra` é deletado.
    """
    if instance.avatar:
        if os.path.isfile(instance.avatar.path):
            os.remove(instance.avatar.path)

class HorasTrabalhadas(models.Model):
	user = models.ForeignKey(Extra, on_delete=models.CASCADE)
	mes = models.CharField(max_length=2)
	ano = models.CharField(max_length=4)
	horas = models.FloatField()
